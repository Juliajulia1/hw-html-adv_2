import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import htmlImport from "gulp-html-import";
import htmlMin from "gulp-htmlmin";
import htmlBemValidator from "gulp-html-bem-validator";
import changed from "gulp-changed";
import { compareContents } from "gulp-changed";
import gGcssMedia from "gulp-group-css-media-queries";
import notify from "gulp-notify";
import plumber from "gulp-plumber";
import concat from "gulp-concat";
import terser from "gulp-terser"
import globImporter from "sass-glob-importer";
import fs from "fs";
// import webp from 'gulp-webp';


const sass = gulpSass(dartSass);
import bsc from "browser-sync";
const browserSync = bsc.create();



const plumberOptions = (title) => {
	return {
		errorHandler: notify.onError({
			title: title,
			message: "Error: <%= error.message %>",
			sound: false
		})
	}


}

const htmlTaskHandler = () => {
	return src("./src/html/*.html")
		.pipe(plumber(plumberOptions("HTML")))
		.pipe(changed("./dist", { hasChanged: compareContents }))
		.pipe(htmlImport('./src/html/components/'))
		.pipe(htmlImport('./src/html/elements/'))
		.pipe(htmlBemValidator())
		.pipe(htmlMin({ collapseWhitespace: true }))
		.pipe(dest("./dist"));
};

const cssTaskHandler = () => {
	return src("./src/scss/style.scss")
		.pipe(plumber(plumberOptions("SCSS")))
		.pipe(changed("./dist/css", { hasChanged: compareContents }))
		.pipe(sass({ importer: globImporter() }).on("error", sass.logError))
		.pipe(gGcssMedia())
		.pipe(autoprefixer())
		.pipe(csso())
		.pipe(dest("./dist/css"))
		.pipe(browserSync.stream());
};

const jsFiles = {
	src: "./src/js/**/*.js",
	dest: "./dist/js",
};

const jsTaskHandler = () => {
	return src(jsFiles.src)
		.pipe(plumber(plumberOptions("JS")))
		.pipe(changed(jsFiles.dest, { hasChanged: compareContents }))
		.pipe(concat("index.js"))
		.pipe(terser())
		.pipe(dest(jsFiles.dest))
		.pipe(browserSync.stream());
};




const imagesTaskHandler = () => {
	return src("./src/images/**/*.*")
		.pipe(changed("./dist/images/**/*", { hasChanged: compareContents }))
		// .pipe(webp())
		.pipe(imagemin({ verbose: true, quality: 90 }))
		.pipe(dest("./dist/images"));
};

// const fontTaskHandler = () => {
// 	return src("./src/fonts/**/*.*").pipe(dest("./dist/fonts"));
// };

const cleanDistTaskHandler = (done) => {
	if (fs.existsSync("./dist")) {
		return src("./dist", { read: false, allowEmpty: true }).pipe(
			clean({ force: true }));
	}
	
	done();
};


const browserSyncConfig = {
	server: {
		baseDir: "./dist",
	},
	
	delay: 600,
	watch: 600,
	notify: false,
};

const watchedFiles = {
	scss: "./src/scss/**/*.scss",
	html: "./src/html/**/*.html",
	img: "./src/img/**/*",
	js: "./src/js/**/*.js",
};

const reloadBrowserSync = (cb) => {
	browserSync.reload();
	cb();
};

const browserSyncTaskHandler = () => {
	browserSync.init(browserSyncConfig);

	watch([watchedFiles.scss]).on("all", series(cssTaskHandler, reloadBrowserSync));
	watch([watchedFiles.html]).on("change", series(htmlTaskHandler, reloadBrowserSync));
	watch([watchedFiles.img]).on("all", series(imagesTaskHandler, reloadBrowserSync));
	watch([watchedFiles.js]).on("all", series(jsTaskHandler, reloadBrowserSync));

};


export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const js = jsTaskHandler;
// export const font = fontTaskHandler;
export const images = imagesTaskHandler;
export const liveReload = browserSyncTaskHandler;


export const build = series(
	cleaning,
	parallel(html, css, js, images) //fontTaskHandler, 
);
export const dev = series(build, liveReload);

